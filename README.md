# API Data Fetcher

Este proyecto proporciona un script en Python que se conecta a la API de datos ANDINO (ckan), realiza una solicitud con parámetros específicos y procesa los resultados en un `DataFrame` de pandas. Los filtros aplicados y el total de resultados también se almacenan en variables para su posterior uso.

El proceso incluye la generación de un mapa con la librería folium, la cual genera un mapa interactivo para navegar los datos en web con cualquier navegador.

## Requisitos

- Python 3.x
- requirements.txt


## Variables

- url (URL portal ANDINO con datastore_search)
- resource_id (ID del recurso a consultar del portal, lo obtienes de la URL del recurso)
- filters (e.g {“key1”: “a”, “key2”: “b”})

### MAPA 

- popup_columns -> Especificar las columnas que se quieren mostrar dentro del popup 

Puedes instalar las librerías necesarias usando pip:

```bash
pip install requirements.txt