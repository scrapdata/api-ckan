import requests
import pandas as pd
import json
import folium
from folium import GeoJson, Popup


url = "http://datos.energia.gob.ar/api/3/action/datastore_search"
resource_id = "80ac25de-a44a-4445-9215-090cf55cfda5"
filters = {"empresabandera": "YPF"}


def fetch_data_from_api(url, resource_id, filters):
    # Parámetros de la solicitud
    params = {
        'resource_id': resource_id,
        'filters': json.dumps(filters)
    }
    
    # Realizar la solicitud a la API
    response = requests.get(url, params=params)
    
    # Verificar si la solicitud fue exitosa
    if response.status_code == 200:
        data = response.json()
        
        # Verificar si se obtuvieron resultados
        if 'result' in data and 'records' in data['result']:
            # Asignar los datos resultantes a un DataFrame de pandas
            df = pd.DataFrame(data['result']['records'])
            
            apiFilters = json.dumps(data['result'].get('filters', {}))
    
            apiTotalResult = data['result'].get('total', 0)
            
            return df, apiFilters, apiTotalResult
        else:
            print("No se obtuvieron resultados de la API")
            return None, None, None
    else:
        print("Error en la solicitud a la API:", response.status_code)
        return None, None, None

def create_map(df, geojson_column, popup_columns, map_center, map_zoom_start):
    # Crear un mapa centrado en un punto específico
    m = folium.Map(tiles='OpenStreetMap',location=map_center, zoom_start=map_zoom_start)

    # Agregar los datos GeoJSON al mapa
    for index, row in df.iterrows():
        geojson_data = json.loads(row[geojson_column])
        
        # Crear el contenido del popup con las columnas especificadas
        popup_content = "<br>".join([f"{col}: {row[col]}" for col in popup_columns])
        popup = Popup(popup_content, max_width=300)
        
        GeoJson(geojson_data, popup=popup).add_to(m)

    # Guardar el mapa en un archivo HTML
    m.save('mapa_interactivo.html')
    return m

df, apiFilters, apiTotalResult = fetch_data_from_api(url, resource_id, filters)

if df is not None:
    ##DATA##
    print("Datos obtenidos:")
    print(df)
    print("Filtros aplicados:", apiFilters)
    print("Total de resultados:", apiTotalResult)
    
    ##MAP##
    # Especificar las columnas a mostrar en el popup
    popup_columns = ['empresabandera','precio','fecha_vigencia']  # Reemplaza columnas del dataframe a trabajar
    # Crear el mapa utilizando la columna 'geojson' del DataFrame
    create_map(df, 'geojson', popup_columns, map_center=[-38, -64], map_zoom_start=5)
else:
    print("No se obtuvieron datos")
